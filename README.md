# OOFirewall

OOFirewall is a minimalist firewall in Rust.

- Filtering using pcap filters
- Do not implement all states yet for complex (TCP) connexions
- Work in progress

# License

```
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
```

# Developer notes

## Time-based bloom filter

Maybe hash implementation already use a (counter/time)-based bloom filter:
https://www.igvita.com/2010/01/06/flow-analysis-time-based-bloom-filters/
But if not, we can use it or even adapt it to use a absolute time as expiration
date for a bit.

## TODO
- Allow related fragmented IP packets (using IP identification field)
- Moar tests
- IPv6, ICMPv6
- Documentation
- Examples
- C bindings + Examples
- Import/export
- Benchmarks
- Winpcap
