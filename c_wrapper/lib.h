/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Small C wrapper around oofirewall */

#ifndef _FIREWALL_H
#define _FIREWALL_H

#define firewall_t void

enum firewall_side {
  FIREWALL_EAST = 0,
  FIREWALL_WEST = 1,
};

firewall_t *firewall_new(void);
void firewall_destroy(firewall_t *firewall);

firewall_t *firewall_import(const char *serialized_firewall);
char *firewall_export(firewall_t *firewall);

int firewall_filter(firewall_t *firewall, uint8 *data, int size, enum firewall_side);

void firewall_set_allow_all(firewall_t *firewall, int allow, enum firewall_side);
int firewall_get_allow_all(firewall_t *firewall, enum firewall_side);

void firewall_set_stateful(firewall_t *firewall, int allow, enum firewall_side);
int firewall_get_stateful(firewall_t *firewall, enum firewall_side);

int firewall_rule_add(firewall_t *firewall, const char *pcap_filter, enum firewall_side);
void firewall_rules_reload(firewall_t *firewall, enum firewall_side);
void firewall_rules_flush(firewall_t *firewall, enum firewall_side);

#endif /* _FIREWALL_H */
