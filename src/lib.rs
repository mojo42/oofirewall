/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

extern crate libc;
extern crate time;
extern crate byteorder;
extern crate linked_hash_map;
extern crate pnet;

use self::time::precise_time_s;
use self::byteorder::{ByteOrder, NetworkEndian};
use self::libc::{c_int, c_char, c_uint, c_uchar, c_ushort, c_long};
use std::ffi::CString;
use linked_hash_map::LinkedHashMap;

use pnet::packet::Packet;
use pnet::packet::ethernet::{EthernetPacket, EtherTypes};
use pnet::packet::ip::IpNextHeaderProtocols;
use pnet::packet::ipv4::Ipv4Packet;
use pnet::packet::udp::UdpPacket;
use pnet::packet::tcp::TcpPacket;
use pnet::packet::icmp::{IcmpPacket, icmp_types};
use pnet::packet::icmp::echo_reply::EchoReplyPacket;
use pnet::packet::icmp::echo_request::EchoRequestPacket;

const UDP_TIMEOUT_MAX: u32 = 30;
const TCP_TIMEOUT_MAX: u32 = 300;
const ICMP_REPLY_TIMEOUT_MAX: u32 = 3;

#[derive(Copy, Clone)]
pub enum Side {
    East = 0,
    West = 1,
}

pub struct Firewall {
    pub east: FwSide,
    pub west: FwSide,
}

impl Firewall {
    pub fn new() -> Firewall {
        Firewall {
            east: FwSide::new(),
            west: FwSide::new(),
        }
    }

    pub fn import(serialized: String) -> Firewall {
        // TODO
        println!("{:?}", serialized);
        Firewall {
            east: FwSide::new(),
            west: FwSide::new(),
        }
    }

    pub fn export(&self) -> String {
        "TODO".to_string()
    }

    // true when allowed, false otherwise
    pub fn filter(&mut self, packet: &[u8], side: Side) -> bool {
        match side {
            Side::East => self.east.filter(packet, &mut self.west),
            Side::West => self.west.filter(packet, &mut self.east),
        }
    }

    pub fn clean_expired(&mut self) -> u64 {
        self.east.clean_expired() +
        self.west.clean_expired()
    }
}

pub struct FwSide {
    udp_v4: ConnexionTable,
    tcp_v4: ConnexionTable,
    icmp: ConnexionTable,
    pub rules: Vec<Rule>,
    pub allow_all: bool,
    pub learn: bool,
}

impl FwSide {
    pub fn new() -> FwSide {
        FwSide {
            udp_v4: ConnexionTable::new(UDP_TIMEOUT_MAX as u64),
            tcp_v4: ConnexionTable::new(TCP_TIMEOUT_MAX as u64),
            icmp: ConnexionTable::new(ICMP_REPLY_TIMEOUT_MAX as u64),
            rules: Vec::new(),
            allow_all: false,
            learn: true,
        }
    }

    fn filter(&mut self, packet: &[u8], other_side: &mut FwSide) -> bool {
        // All all on this side ?
        let mut go = self.allow_all;

        // Rule matching ?
        if !go {
            for r in self.rules.iter() {
                if r.filter(packet) {
                    go = true;
                    break;
                }
            }
        }

        // Related connexions and learning
        if !go && other_side.learn {
            go = self.related(&packet, other_side);
        } else if go && self.learn {
            self.learn(&packet);
        }

        return go;
    }

    fn clean_expired(&mut self) -> u64 {
        self.udp_v4.clean_expired() +
        self.tcp_v4.clean_expired() +
        self.icmp.clean_expired()
    }

    fn related(&mut self, packet: &[u8], os: &mut FwSide) -> bool {
        println!("lookup related connexions");
        // Fragmented packet ?
        // if ((DF == 0 && offset == 0) || DF == 1) {
            match EthernetPacket::new(&packet) {
                Some(eth) => match eth.get_ethertype() {
                    EtherTypes::Ipv4 => match Ipv4Packet::new(eth.payload()) {
                        Some(ip) => match ip.get_next_level_protocol() {
                            IpNextHeaderProtocols::Udp => match UdpPacket::new(ip.payload()) {
                                Some(udp) => os.udp_v4.lookup(FwSide::udp_v4_hash_rev(&ip, &udp)),
                                None => false,
                            },
                            IpNextHeaderProtocols::Tcp => match TcpPacket::new(ip.payload()) {
                                Some(tcp) => os.tcp_v4.lookup(FwSide::tcp_v4_hash_rev(&ip, &tcp)),
                                None => false,
                            },
                            IpNextHeaderProtocols::Icmp => match IcmpPacket::new(ip.payload()) {
                                Some(icmp) => match icmp.get_icmp_type() {
                                    icmp_types::EchoReply => match EchoReplyPacket::new(packet) {
                                        Some(rep) => os.icmp.lookup(FwSide::icmp_echo_reply_hash(&ip, &rep)),
                                        None => false,
                                    },
                                    _ => false
                                },
                                None => false,
                            },
                            _ => false,
                        },
                        None => false,
                    },
                    _ => false,
                },
                None => false,
            }
        // }
        // if go && type_IP && DF == 0 && offset == 0 {
        //      add_to_ip_frag_learn_table();
        // } else if type_IP && DF == 0 && offset != 0 && lookup_in_ip_frag_lean_table() {
        //      go = true;
        //      if  more_fragments == 0 {
        //          delete_in_ip_frag_learn_table();
        //      }
        // }
    }

    fn learn(&mut self, packet: &[u8]) {
        println!("learn");
        match EthernetPacket::new(&packet) {
            Some(eth) => match eth.get_ethertype() {
                EtherTypes::Ipv4 => match Ipv4Packet::new(eth.payload()) {
                    Some(ip) => match ip.get_next_level_protocol() {
                        IpNextHeaderProtocols::Udp => match UdpPacket::new(ip.payload()) {
                            Some(udp) => self.udp_v4.insert(FwSide::udp_v4_hash(&ip, &udp)),
                            None => {},
                        },
                        IpNextHeaderProtocols::Tcp => match TcpPacket::new(ip.payload()) {
                            Some(tcp) => self.tcp_v4.insert(FwSide::tcp_v4_hash(&ip, &tcp)),
                            None => {},
                        },
                        IpNextHeaderProtocols::Icmp => match IcmpPacket::new(ip.payload()) {
                            Some(icmp) => match icmp.get_icmp_type() {
                                icmp_types::EchoRequest => match EchoRequestPacket::new(packet) {
                                    Some(req) => self.icmp.insert(FwSide::icmp_echo_request_hash(&ip, &req)),
                                    None => {},
                                },
                                _ => {}
                            },
                            None => {},
                        },
                        _ => {},
                    },
                    None => {},
                },
                _ => {},
            },
            None => {},
        }
    }

    fn hash_u32_u16(a: u32, b: u32, x: u16, y: u16) -> u64 {
        let m = ((a as u64) << 32) & (b as u64);
        let n = ((x as u64) << 16) & (y as u64);
        m ^ (n << 16)
    }

    fn udp_v4_hash(ip: &Ipv4Packet, udp: &UdpPacket) -> u64 {
        FwSide::hash_u32_u16(NetworkEndian::read_u32(&ip.get_source().octets()),
                             NetworkEndian::read_u32(&ip.get_destination().octets()),
                             udp.get_source(), udp.get_destination())
    }

    fn udp_v4_hash_rev(ip: &Ipv4Packet, udp: &UdpPacket) -> u64 {
        FwSide::hash_u32_u16(NetworkEndian::read_u32(&ip.get_destination().octets()),
                             NetworkEndian::read_u32(&ip.get_source().octets()),
                             udp.get_destination(), udp.get_source())
    }

    fn tcp_v4_hash(ip: &Ipv4Packet, tcp: &TcpPacket) -> u64 {
        FwSide::hash_u32_u16(NetworkEndian::read_u32(&ip.get_source().octets()),
                             NetworkEndian::read_u32(&ip.get_destination().octets()),
                             tcp.get_source(), tcp.get_destination())
    }

    fn tcp_v4_hash_rev(ip: &Ipv4Packet, tcp: &TcpPacket) -> u64 {
        FwSide::hash_u32_u16(NetworkEndian::read_u32(&ip.get_destination().octets()),
                             NetworkEndian::read_u32(&ip.get_source().octets()),
                             tcp.get_destination(), tcp.get_source())
    }

    fn icmp_echo_request_hash(ip: &Ipv4Packet, req: &EchoRequestPacket) -> u64 {
        FwSide::hash_u32_u16(NetworkEndian::read_u32(&ip.get_source().octets()),
                             NetworkEndian::read_u32(&ip.get_destination().octets()),
                             req.get_identifier(), req.get_sequence_number())
    }

    fn icmp_echo_reply_hash(ip: &Ipv4Packet, rep: &EchoReplyPacket) -> u64 {
        FwSide::hash_u32_u16(NetworkEndian::read_u32(&ip.get_destination().octets()),
                             NetworkEndian::read_u32(&ip.get_source().octets()),
                             rep.get_identifier(), rep.get_sequence_number())
    }
}

struct ConnexionTable {
    // Quickly find random value (hash, epoch date in seconds)
    hash_to_time: LinkedHashMap<u64, u64>,
    timeout: u64,
}

impl ConnexionTable {
    fn new(timeout_s: u64) -> ConnexionTable {
        ConnexionTable {
            hash_to_time: LinkedHashMap::new(),
            timeout: timeout_s,
        }
    }

    fn lookup(&mut self, key: u64) -> bool {
        match self.hash_to_time.get_mut(&key) {
            None => return false,
            Some(t) => {
                let clock = precise_time_s() as u64;
                if clock < *t {
                    *t = clock + self.timeout;
                } else {
                    return false;
                }
            }
        }
        self.hash_to_time.get_refresh(&key);
        true
    }

    // Insert new key or renew if already exists
    fn insert(&mut self, key: u64) {
        self.hash_to_time.insert(key, self.timeout + precise_time_s() as u64);
    }

    fn clean_expired(&mut self) -> u64 {
        let mut cnt: u64 = 0;
        let clock = precise_time_s() as u64;
        loop {
            match self.hash_to_time.front() {
                None => { return cnt; },
                Some((_, &t)) => {
                    if clock >= t {
                        self.hash_to_time.pop_front();
                        cnt += 1;
                    } else {
                        return cnt;
                    }
                },
            };
        }
    }
}

pub struct Rule {
    pub pcap_filter: String,
    bpf_program: Struct_bpf_program,
}

impl Rule {
    pub fn new(pcap_filter: String) -> Result<Rule, String> {
        let mut r = Rule {
            pcap_filter: pcap_filter.clone(),
            bpf_program: Struct_bpf_program {
                bf_len: 0,
                bf_insns: 0 as *mut Struct_bpf_insn,
            },
        };
        let filter = CString::new(pcap_filter.as_str()).unwrap();
        unsafe {
            if pcap_compile_nopcap(65535, // snaplen, max IP packet len
                                   DLT_EN10MB, // linktype
                                   &mut r.bpf_program, // result program
                                   filter.as_ptr(), // our pcap filter
                                   1, // optimize please
                                   0xffffffff // netmask unknown
                                   ) < 0 {
                return Err("Filter does not compile".to_string());
            }
        }
        Ok(r)
    }

    pub fn filter(&self, packet: &[u8]) -> bool {
        let l = packet.len() as u32;
        let mut header = Struct_pcap_pkthdr {
            ts: Struct_timeval {
                tv_sec: 0,
                tv_usec: 0,
            },
            caplen: l,
            len: l,
        };
        unsafe {
            // 0 => doesn't match
            // _ => match
            pcap_offline_filter(&self.bpf_program, &mut header, &packet[0] as *const u8) != 0
        }
    }
}

impl Drop for Rule {
    fn drop(&mut self) {
        unsafe {
            pcap_freecode(&mut self.bpf_program);
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Struct_bpf_program {
    pub bf_len: c_uint,
    pub bf_insns: *mut Struct_bpf_insn,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Struct_bpf_insn {
    pub code: c_ushort,
    pub jt: c_uchar,
    pub jf: c_uchar,
    pub k: c_uint,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Struct_pcap_pkthdr {
    pub ts: Struct_timeval,
    pub caplen: c_uint,
    pub len: c_uint,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Struct_timeval {
    pub tv_sec: c_long,
    pub tv_usec: c_long,
}

const DLT_EN10MB: i32 = 1;

#[cfg(target_os = "windows")]
#[link(name = "wpcap")]
extern {
    // TODO
}

#[cfg(not(target_os = "windows"))]
#[link(name = "pcap")]
extern {
    fn pcap_compile_nopcap(snaplen: c_int,
                           linktype: c_int,
                           fp: *mut Struct_bpf_program,
                           string: *const c_char,
                           optimize: c_int,
                           netmask: c_uint) -> c_int;

    fn pcap_freecode(fp: *mut Struct_bpf_program);

    fn pcap_offline_filter(fp: *const Struct_bpf_program,
                           h: *mut Struct_pcap_pkthdr,
                           // original: pkt: *const c_char
                           pkt: *const u8) -> c_int;
}
