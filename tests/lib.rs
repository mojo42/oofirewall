/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

extern crate oofirewall;
mod packets;

#[cfg(test)]
mod tests {
    use packets::tests::*;
    use oofirewall::{Firewall, Side, Rule};
    use std::time::Duration;
    use std::thread;

    fn bad_packets(firewall: &mut Firewall, filter_result: bool, side: Side) {
                assert!(firewall.filter(PACKET_EMPTY, side) == filter_result);
                assert!(firewall.filter(PACKET_TINY, side) == filter_result);
                assert!(firewall.filter(PACKET_ETH, side) == filter_result);
                assert!(firewall.filter(PACKET_STRANGE, side) == filter_result);
                assert!(firewall.filter(PACKET_LOL, side) == filter_result);
                assert!(firewall.filter(PACKET_BIG, side) == filter_result);
    }

    #[test]
    fn allow_all() {
        let mut fw = Firewall::new();
        fw.east.learn = false;
        fw.west.learn = false;
        fw.east.allow_all = true;
        fw.west.allow_all = true;
        bad_packets(&mut fw, true, Side::East);
        bad_packets(&mut fw, true, Side::West);
        fw.east.allow_all = false;
        bad_packets(&mut fw, false, Side::East);
        bad_packets(&mut fw, true, Side::West);
        fw.west.allow_all = false;
        bad_packets(&mut fw, false, Side::East);
        bad_packets(&mut fw, false, Side::West);
        fw.east.allow_all = true;
        bad_packets(&mut fw, true, Side::East);
        bad_packets(&mut fw, false, Side::West);
        fw.west.allow_all = true;
        bad_packets(&mut fw, true, Side::East);
        bad_packets(&mut fw, true, Side::West);
    }

    #[test]
    fn rule_filter() {
        let mut fw = Firewall::new();
        fw.east.learn = false;
        fw.west.learn = false;
        fw.east.allow_all = false;
        fw.west.allow_all = false;
        fw.east.rules.push(Rule::new("ether src a4:5e:60:d2:30:bf".to_string()).unwrap());
        assert!(fw.filter(PACKET_PING, Side::East) == true);
        assert!(fw.filter(PACKET_PING, Side::West) == false);
        assert!(fw.filter(PACKET_PONG, Side::East) == false);
        assert!(fw.filter(PACKET_PONG, Side::West) == false);
        fw.east.rules.clear();
        assert!(fw.filter(PACKET_PING, Side::East) == false);
        assert!(fw.filter(PACKET_PING, Side::West) == false);
        assert!(fw.filter(PACKET_PONG, Side::East) == false);
        assert!(fw.filter(PACKET_PONG, Side::West) == false);
        fw.east.rules.push(Rule::new("ip src 192.168.1.12".to_string()).unwrap());
        assert!(fw.filter(PACKET_PING, Side::East) == true);
        assert!(fw.filter(PACKET_PING, Side::West) == false);
        assert!(fw.filter(PACKET_PONG, Side::East) == false);
        assert!(fw.filter(PACKET_PONG, Side::West) == false);
        fw.east.rules.clear();
        assert!(fw.filter(PACKET_PING, Side::East) == false);
        assert!(fw.filter(PACKET_PING, Side::West) == false);
        assert!(fw.filter(PACKET_PONG, Side::East) == false);
        assert!(fw.filter(PACKET_PONG, Side::West) == false);
        fw.east.rules.push(Rule::new("icmp".to_string()).unwrap());
        assert!(fw.filter(PACKET_PING, Side::East) == true);
        assert!(fw.filter(PACKET_PING, Side::West) == false);
        assert!(fw.filter(PACKET_PONG, Side::East) == true);
        assert!(fw.filter(PACKET_PONG, Side::West) == false);
    }

    #[test]
    fn rule_filter_speed() {
        // TODO
        // Trace F(number_of_filter) = pps ?
    }

    #[test]
    fn learn_udp() {
        // Without learning, packet is able to escape but response is dropped
        let mut fw = Firewall::new();
        fw.east.learn = false;
        fw.west.learn = false;
        fw.east.allow_all = true;
        assert!(fw.filter(PACKET_UDP_0, Side::East) == true);
        assert!(fw.filter(PACKET_UDP_1, Side::West) == false);
        assert!(fw.clean_expired() == 0);
        // With learning, related UDP packets are allowed
        fw.east.learn = true;
        assert!(fw.filter(PACKET_UDP_0, Side::East) == true);
        assert!(fw.filter(PACKET_UDP_1, Side::West) == true);
        assert!(fw.filter(PACKET_UDP_1, Side::West) == true);
        assert!(fw.filter(PACKET_UDP_1, Side::West) == true);
        // 30 seconds timeout for UDP
        thread::sleep(Duration::from_millis(29_000));
        assert!(fw.clean_expired() == 0);
        thread::sleep(Duration::from_millis(1_000));
        assert!(fw.filter(PACKET_UDP_1, Side::West) == false);
        assert!(fw.clean_expired() == 1);
        assert!(fw.clean_expired() == 0);
    }

    // TODO: add this when test crate will be stable
    /*
    #[bench]
    fn learn_udp_speed(b: &mut Bencher) {
        let mut fw = Firewall::new();
        fw.east.learn = true;
        fw.west.learn = false;
        fw.east.allow_all = true;
        assert!(fw.filter(PACKET_UDP_1, Side::East) == true);
        b.iter(|| {
            fw.filter(PACKET_UDP_2, Side::West)
        });
    }
    */

    #[test]
    fn learn_tcp() {
        // Without learning, packet is able to escape but response is dropped
        let mut fw = Firewall::new();
        fw.east.learn = false;
        fw.west.learn = false;
        fw.east.allow_all = true;
        assert!(fw.filter(PACKET_TCP_0_CS, Side::East) == true);
        assert!(fw.filter(PACKET_TCP_1_SC, Side::West) == false);
        assert!(fw.filter(PACKET_TCP_2_CS, Side::East) == true);
        assert!(fw.filter(PACKET_TCP_3_SC, Side::West) == false);
        assert!(fw.filter(PACKET_TCP_4_CS, Side::East) == true);
        assert!(fw.filter(PACKET_TCP_5_CS, Side::East) == true);
        assert!(fw.filter(PACKET_TCP_6_SC, Side::West) == false);
        assert!(fw.filter(PACKET_TCP_7_CS, Side::East) == true);
        assert!(fw.filter(PACKET_TCP_0_CS, Side::East) == true);
        assert!(fw.filter(PACKET_TCP_1_SC, Side::West) == false);
        assert!(fw.clean_expired() == 0);
        // With learning, related TCP packets are allowed
        fw.east.learn = true;
        assert!(fw.filter(PACKET_TCP_0_CS, Side::East) == true);
        assert!(fw.filter(PACKET_TCP_1_SC, Side::West) == true);
        assert!(fw.filter(PACKET_TCP_2_CS, Side::East) == true);
        assert!(fw.filter(PACKET_TCP_3_SC, Side::West) == true);
        assert!(fw.filter(PACKET_TCP_4_CS, Side::East) == true);
        assert!(fw.filter(PACKET_TCP_5_CS, Side::East) == true);
        assert!(fw.filter(PACKET_TCP_6_SC, Side::West) == true);
        assert!(fw.filter(PACKET_TCP_7_CS, Side::East) == true);
        // 300 seconds timeout for TCP
        thread::sleep(Duration::from_millis(298_000));
        assert!(fw.clean_expired() == 0);
        thread::sleep(Duration::from_millis(2_000));
        assert!(fw.filter(PACKET_TCP_3_SC, Side::West) == false);
        assert!(fw.clean_expired() == 1);
        assert!(fw.clean_expired() == 0);
    }

    #[test]
    fn learn_icmp() {
        // Without learning, packet is able to escape but response is dropped
        let mut fw = Firewall::new();
        fw.east.learn = false;
        fw.west.learn = false;
        fw.east.allow_all = true;
        assert!(fw.filter(PACKET_PING, Side::East) == true);
        assert!(fw.filter(PACKET_PONG, Side::West) == false);
        assert!(fw.clean_expired() == 0);
        // With learning, related ICMP packets are allowed
        fw.east.learn = true;
        assert!(fw.filter(PACKET_PING, Side::East) == true);
        assert!(fw.filter(PACKET_PONG, Side::West) == true);
        // Wait for timeout
        thread::sleep(Duration::from_millis(2_000));
        assert!(fw.clean_expired() == 0);
        thread::sleep(Duration::from_millis(1_000));
        assert!(fw.filter(PACKET_PONG, Side::West) == false);
        assert!(fw.clean_expired() == 1);
        assert!(fw.clean_expired() == 0);
    }
}
